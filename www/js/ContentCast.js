var CC = (function($){
  var CC = {};
  var feed = {};

  CC.loadRss = function loadRss(url, callback) {

    // ajax get purl
    /*
    $.ajax({
      dataType: "xml",
      url: 'Sammelaktion.rss',
      async: true,
      success: function(data) {
        console.log(data);
      }
    });
    */
    fetch(url)
    .then(function(response){
      feed.url = response.url;
      return response.text();
    })
    .then(function(text){
      return $.parseXML(text);
    })
    .then(function(xmlDoc){

      var xml = $(xmlDoc);

      feed.title = xml.find('title').first().text();
      feed.items = [];

      // for each item create a JSON entry
      // AND add fields top, bottom, left, right
      xml.find('item').each(function(i){
        // item ID to be added to feed.items
        var item = {};
        itemId = $(this).find('guid').text();
        if( localStorage.getItem(itemId) !== null){
          item = JSON.parse(localStorage.getItem(itemId));
        }
        item.id = itemId;
        if(!item.title){
          // local tops remote
          item.title = $(this).find('title').text();
        }
        item.description = $(this).find('description').text();
        item.enclosure = {};
        item.enclosure.url = $(this).find('enclosure').attr('url');
        item.enclosure.type = $(this).find('enclosure').attr('type');
        item.enclosure.length = $(this).find('enclosure').attr('length');
        item.thumbnail = $(this).find('media\\:thumbnail').attr('url');
        //console.log(i, item);
        feed.items.push(item);

      });

      console.log(feed);
      // store feed and items in localstorage
      storeFeed();

      return;

    })
    .then(function(){
      callback();
    });


  };

  function storeFeed(){
    var feedKey = feed.url;
    var feedValue = [];

    $(feed.items).each(function(){
      var itemKey = this.id;
      var itemValue = JSON.stringify(this);
      feedValue.push(itemKey);
      localStorage.setItem(itemKey, itemValue);
    });

    localStorage.setItem(feedKey, JSON.stringify(feedValue));

  };

  function readFeed(url){
    feed = {};
    feed.url = url;
    feed.items = [];

    feedValue = JSON.parse(localStorage.getItem(url));
    feedValue.each(function(){
      feed.items.push(JSON.parse(localStorage.getItem(this)));
    });

    console.log(feed);
  };

  CC.update = function(itemId, o){
    // load item from localstorage

    // copy fields from o to items

    // save item to loacalstorage

  };

  CC.export = function(url){
    //
    function storeFile(name, data){
      // TODO data from localstorage
      function writeFile(fileEntry, data) {
        // Create a FileWriter object for our FileEntry (log.txt).
        fileEntry.createWriter(function (fileWriter) {

          fileWriter.onwriteend = function() {
            console.log("Successful file write...");
          };

          fileWriter.onerror = function (e) {
            console.log("Failed file write: " + e.toString());
          };

          // If data object is not passed in,
          // create a new Blob instead.
          if (!data) {
            data = new Blob(['some file data'], { type: 'text/plain' });
          }

          fileWriter.write(data);
        });
      }

      window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {

        console.log('file system open: ' + fs.name);
        console.log(fs.root);
        fs.root.getFile(name, { create: true, exclusive: false }, function (fileEntry) {

          console.log("fileEntry is file? " + fileEntry.isFile.toString() + " !");
          console.log(fileEntry.name);
          console.log(fileEntry.fullPath);
          // fileEntry.name == 'someFile.txt'
          // fileEntry.fullPath == '/someFile.txt'
          writeFile(fileEntry, data);

        }, onErrorCreateFile);

      }, onErrorLoadFs);
    }

  };

  function doImport(url){

  };

  CC.feed = feed;
  return CC;
}(jQuery));
