console.log("Loading template.");
$.ajax({
  dataType: "html",
  url: 'tpl/ContentCard.tpl.html',
  async: false,
  success: function(data) {
    $('head').prepend(data);
    //console.log(data);
  }
});

customElements.define('content-card',
class extends HTMLElement {
  constructor() {
    super();

    let template = document.getElementById('template-content-card');
    let templateContent = template.content;
    const shadow = this.attachShadow({mode: 'open'}).appendChild(templateContent.cloneNode(true));

  }

  connectedCallback(){
    var cardId = this.getAttribute('id');
    var cardData = JSON.parse(localStorage.getItem(cardId));

    var shadow = this.shadowRoot;
    shadow.querySelectorAll('.title')[0].append(cardData.title);
    shadow.querySelectorAll('img')[0].src = cardData.enclosure.url;
    shadow.querySelectorAll('.left')[0].append(cardData.left || "left");
    shadow.querySelectorAll('.right')[0].append(cardData.right || "right");
    shadow.querySelectorAll('.top')[0].append(cardData.top || "top");
    shadow.querySelectorAll('.bottom')[0].append(cardData.bottom || "bottom");

    $(shadow.querySelectorAll('.field')).on('click', function(e){
      // no card onclick action when clicking field
      e.stopPropagation();
    });
    var saver;
    $(shadow.querySelectorAll('.field')).on('keyup', function(){
      if(saver){
        clearTimeout(saver);
      }
      saver = setTimeout(function(){
        console.log("do saving");
        saveState();
      }, 1000);
      console.log("scheduled saver");
    });

    function saveState(){
      cardData.title = shadow.querySelectorAll('.title')[0].textContent;
      cardData.left = shadow.querySelectorAll('.left')[0].textContent;
      cardData.right = shadow.querySelectorAll('.right')[0].textContent;
      cardData.top = shadow.querySelectorAll('.top')[0].textContent;
      cardData.bottom = shadow.querySelectorAll('.bottom')[0].textContent;
      console.log(cardData);
      // TODO call CC.update instead? Card or API concern?
      localStorage.setItem(cardId, JSON.stringify(cardData));
    }
  }

});
