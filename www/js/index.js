window.onload = function() {
  if('serviceWorker' in navigator){
    navigator.serviceWorker
    .register('./service-worker.js', { scope: './'} )
    .then(function(registration){
      console.log("ServiceWorker registered");

      loadSammelaktion();

    })
    .catch(function(err){
      console.log("Error registering service worker", err);
    });
  }
  else {
    console.log("ServiceWorker not available.");
  }


  function loadSammelaktion(){
    console.log("trying to load Sammelaktion");
    CC.loadRss('https://shani.schwichtenberg.net/Podcast/Mika/Bilder/Sammelaktion/', showfeed);
  }
}

//document.execCommand("defaultParagraphSeparator", false, "br");

function showfeed(){
  $(CC.feed.items).each(function(){
    //$('div.carddeck').append('<img src="' + this.thumbnail + '" class="img-fluid">');
    $('div.carddeck').append('<content-card id="' + this.id + '">')
  });
  $('content-card:first').addClass('active');

  $('content-card').on('click', function(){
    nextCard();
  });
  var delayed;
  $('content-card').on('mousedown', function(){
    delayed = setTimeout(function(){
      prevCard();
    }, 1000);
  });
  $('content-card').on('mouseup', function(){
    clearTimeout(delayed);
  });
}

function thisCard(id){
  $($('content-card.active')[0]).removeClass('active');
  $('#' + id).addClass('active');
}

function nextCard(){
  $('content-card.active + content-card').addClass('active');
  $($('content-card.active')[0]).removeClass('active');
}

function prevCard(){
  $('content-card:has(+ content-card.active)').addClass('active');
  $($('content-card.active')[1]).removeClass('active');
}
