var cacheName = "v1";

self.addEventListener('install', function(e){
  console.log("[ServiceWorker] Installed");

});

self.addEventListener('activate', function(e){
  console.log("[ServiceWorker] Activated");

  e.waitUntil(self.clients.claim());

});

self.addEventListener('fetch', e => {
  //console.log("[ServiceWorker] Fetch", e.request.url);

  e.respondWith(async function(){
    const cachedResponse = await caches.match(e.request);
    if( cachedResponse ){
      console.log("[ServiceWorker] Respond from cache", e.request.url);
      return cachedResponse;
    }

    //console.log("[ServiceWorker] Fetching", e.request.url);
    return fetch(e.request);
  }());

  if( e.request.url.substr(e.request.url.indexOf('/'), 11) !== "//localhost" ){
    console.log("[ServiceWorker] Caching ...", e.request.url);
    caches.open(cacheName).then(function(cache) {
      //cache.add( e.request );
      var r = new Request(e.request.url, {mode: 'cors'});
      cache.add(r).then(function(){
        console.log("[ServiceWorker] Cached ...", e.request.url);
      });
    });
  }

});

/*
self.addEventListener('fetch', function(e){
  console.log("[ServiceWorker] Fetch", e.request.url);

  e.respondWith(
    caches.match(e.request).then((response) => {
      console.log("respond with");

      if(response){
        console.log("[ServiceWorker] respond from cache", e.request.url);
        return response;
      }

      var requestClone = e.request.clone();
      fetch(e.request)
      .then(function(response) {

        if ( !response ) {
          console.log("[ServiceWorker] No response from fetch ")
          return response;
        }

        console.log(response);

        var responseClone = response.clone();

        console.log(responseClone);

        caches.open(cacheName).then(function(cache) {
          cache.put(requestClone, responseClone);
          console.log('[ServiceWorker] New Data Cached', e.request.url);
          return response;
        });

      })
      .catch(function(err) {
        console.log('[ServiceWorker] Error Fetching & Caching New Data', err);
      });

    })

  );
});
*/
